
CREATE SCHEMA IF NOT EXISTS `mydb` DEFAULT CHARACTER SET utf8 ;
USE `mydb` ;

-- -----------------------------------------------------
-- Table `mydb`.`credit_card`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`credit_card` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `numbers` VARCHAR(45) NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `id_UNIQUE` (`id` ASC));


-- -----------------------------------------------------
-- Table `mydb`.`password`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`secure_password` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `password_str` VARCHAR(16) NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `id_UNIQUE` (`id` ASC));


-- -----------------------------------------------------
-- Table `mydb`.`business_customer`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`business_customer` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `business_customer_name` VARCHAR(45) NOT NULL,
  `phone_number` VARCHAR(10) NOT NULL,
  `password_id` INT(11) NOT NULL,
  `credit_card_id` INT(11) NOT NULL,
  PRIMARY KEY (`id`, `password_id`, `credit_card_id`),
  UNIQUE INDEX `id_UNIQUE` (`id` ASC),
  UNIQUE INDEX `business_customer_name_UNIQUE` (`business_customer_name` ASC),
  INDEX `fk_business_customer_password1_idx` (`password_id` ASC),
  INDEX `fk_business_customer_credit_card1_idx` (`credit_card_id` ASC),
  CONSTRAINT `fk_business_customer_credit_card1`
    FOREIGN KEY (`credit_card_id`)
    REFERENCES `mydb`.`credit_card` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_business_customer_password1`
    FOREIGN KEY (`password_id`)
    REFERENCES `mydb`.`secure_password` (`id`)
    ON UPDATE CASCADE);


-- -----------------------------------------------------
-- Table `mydb`.`order`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`order_detail` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `start_point` VARCHAR(45) NOT NULL,
  `end_point` VARCHAR(45) NOT NULL,
  `city` VARCHAR(20) NOT NULL,
  `price`SMALLINT(250) NOT NULL,
  `payment` ENUM('credit_card', 'cash') NOT NULL,
  `date_of_order` VARCHAR(8) NOT NULL,
  `rating` SMALLINT(5) NULL DEFAULT NULL,
  `status_of_order` TINYINT NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `id_UNIQUE` (`id` ASC));


-- -----------------------------------------------------
-- Table `mydb`.`business_customer_has_order`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`business_customer_has_order_detail` (
  `business_customer_id` INT(11) NOT NULL,
  `order_detail_id` INT(11) NOT NULL,
  PRIMARY KEY (`business_customer_id`, `order_detail_id`),
  INDEX `fk_business_customer_has_order_order1_idx` (`order_detail_id` ASC),
  INDEX `fk_business_customer_has_order_business_customer1_idx` (`business_customer_id` ASC),
  CONSTRAINT `fk_business_customer_has_order_business_customer1`
    FOREIGN KEY (`business_customer_id`)
    REFERENCES `mydb`.`business_customer` (`id`)
    ON DELETE CASCADE,
  CONSTRAINT `fk_business_customer_has_order_order1`
    FOREIGN KEY (`order_detail_id`)
    REFERENCES `mydb`.`order_detail` (`id`)
    ON DELETE CASCADE);


-- -----------------------------------------------------
-- Table `mydb`.`driver`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`driver` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `driver_name` VARCHAR(45) NOT NULL,
  `phone_number` VARCHAR(10) NOT NULL,
  `driver_status` TINYINT NOT NULL,
  `average_rating` SMALLINT(5) NULL DEFAULT NULL,
  `credit_card_id` INT(11) NOT NULL,
  `password_id` INT(11) NOT NULL,
  PRIMARY KEY (`id`, `credit_card_id`, `password_id`),
  UNIQUE INDEX `id_UNIQUE` (`id`, `password_id` ASC),
  INDEX `fk_user_table1_idx` (`password_id` ASC),
  INDEX `fk_driver_credit_card1_idx` (`credit_card_id` ASC),
  CONSTRAINT `fk_driver_credit_card1`
    FOREIGN KEY (`credit_card_id`)
    REFERENCES `mydb`.`credit_card` (`id`)
    ON UPDATE CASCADE,
  CONSTRAINT `fk_user_table10`
    FOREIGN KEY (`password_id`)
    REFERENCES `mydb`.`secure_password` (`id`)
    ON UPDATE CASCADE);


-- -----------------------------------------------------
-- Table `mydb`.`car`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`car` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `brand` VARCHAR(20) NOT NULL,
  `model` VARCHAR(20) NOT NULL,
  `class` ENUM('wagon', 'comfort', 'minibus', 'business') NOT NULL,
  `driver_id` INT(11) NOT NULL,
  PRIMARY KEY (`id`, `driver_id`),
  UNIQUE INDEX `id_UNIQUE` (`id` ASC),
  INDEX `fk_car_driver1_idx` (`driver_id` ASC),
  CONSTRAINT `fk_car_driver1`
    FOREIGN KEY (`driver_id`)
    REFERENCES `mydb`.`driver` (`id`)
    ON UPDATE CASCADE);

-- -----------------------------------------------------
-- Table `mydb`.`driver_has_order`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`driver_has_order_detail` (
  `driver_id` INT(11) NOT NULL,
  `order_detail_id` INT(11) NOT NULL,
  PRIMARY KEY (`driver_id`, `order_detail_id`),
  INDEX `fk_driver_has_order_driver1_idx` (`driver_id` ASC),
  INDEX `fk_driver_has_order_order1_idx` (`order_detail_id` ASC),
  CONSTRAINT `fk_driver_has_order_driver1`
    FOREIGN KEY (`driver_id`)
    REFERENCES `mydb`.`driver` (`id`)
    ON UPDATE CASCADE,
  CONSTRAINT `fk_driver_has_order_order1`
    FOREIGN KEY (`order_detail_id`)
    REFERENCES `mydb`.`order_detail` (`id`)
    ON UPDATE CASCADE);


-- -----------------------------------------------------
-- Table `mydb`.`incident`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`incident` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `incident_name` VARCHAR(30) NOT NULL,
  `incident_status` TINYINT NOT NULL,
  `car_id` INT(11) NOT NULL,
  PRIMARY KEY (`id`, `car_id`),
  UNIQUE INDEX `id_UNIQUE` (`id` ASC),
  INDEX `fk_incident_car1_idx` (`car_id` ASC),
  CONSTRAINT `fk_incident_car1`
    FOREIGN KEY (`car_id`)
    REFERENCES `mydb`.`car` (`id`)
    ON UPDATE CASCADE);


-- -----------------------------------------------------
-- Table `mydb`.`order_condition`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`order_detail_condition` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `order_detail_condition` ENUM('courier', 'sign', 'driver', 'pet', 'baggage', 'conditioner', 'english', 'smoker', 'silence') NULL DEFAULT NULL,
  `order_detail_id` INT(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `id_UNIQUE` (`id` ASC),
  INDEX `fk_order_condition_order1_idx` (`order_detail_id` ASC),
  CONSTRAINT `fk_order_condition_order1`
    FOREIGN KEY (`order_detail_id`)
    REFERENCES `mydb`.`order_detail` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION);


-- -----------------------------------------------------
-- Table `mydb`.`report`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`report` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `report_name` VARCHAR(45) NOT NULL,
  `driver_id` INT(11) NOT NULL,
  PRIMARY KEY (`id`, `driver_id`),
  UNIQUE INDEX `id_UNIQUE` (`id` ASC),
  INDEX `fk_report_driver1_idx` (`driver_id` ASC),
  CONSTRAINT `fk_report_driver1`
    FOREIGN KEY (`driver_id`)
    REFERENCES `mydb`.`driver` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE);

-- -----------------------------------------------------
-- Table `mydb`.`user`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`customer` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `customer_name` VARCHAR(45) NOT NULL,
  `phone_number` VARCHAR(10) NOT NULL,
  `password_id` INT(11) NOT NULL,
  `credit_card_id` INT(11),
  PRIMARY KEY (`id`, `password_id`),
  UNIQUE INDEX `id_UNIQUE` (`id`, `password_id` ASC),
  INDEX `fk_user_credit_card1_idx` (`credit_card_id` ASC),
  INDEX `fk_user_password1_idx` (`password_id` ASC),
  CONSTRAINT `fk_user_credit_card1`
    FOREIGN KEY (`credit_card_id`)
    REFERENCES `mydb`.`credit_card` (`id`)
    ON UPDATE CASCADE,
  CONSTRAINT `fk_user_password1`
    FOREIGN KEY (`password_id`)
    REFERENCES `mydb`.`secure_password` (`id`)
    ON UPDATE CASCADE);


-- -----------------------------------------------------
-- Table `mydb`.`user_has_order`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`customer_has_order_detail` (
  `customer_id` INT(11) NOT NULL,
  `order_detail_id` INT(11) NOT NULL,
  PRIMARY KEY (`customer_id`, `order_detail_id`),
  INDEX `fk_customer_has_order_order1_idx` (`order_detail_id` ASC),
  INDEX `fk_customer_has_order_customer1_idx` (`customer_id` ASC),
  CONSTRAINT `fk_customer_has_order_order1`
    FOREIGN KEY (`order_detail_id`)
    REFERENCES `mydb`.`order_detail` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_customer_has_order_customer1`
    FOREIGN KEY (`customer_id`)
    REFERENCES `mydb`.`customer` (`id`)
    ON DELETE CASCADE);


INSERT INTO credit_card (numbers)
  VALUES
  ("1487765487464535"),
  ("3836747474746389"),
  ("3738482193847665"),
  ("4823748973246782"),
  ("5743987589435543"),
  ("3271893776543857"),
  ("4230875894356438"),
  ("4239874632478643"),
  ("5874598239845648"),
  ("5768932462346557"),
  ("5438975439656543"),
  ("3672183678216464"),
  ("5433435345575438");
INSERT INTO secure_password (password_str) 
  VALUES
  ("sdhajdhsauth"),
  ("dhsjakhdjkash"),
  ("ghjhgbdfutuyy"),
  ("gdkjfnsduotiy"),
  ("password12356"),
  ("123qwerty123"),
  ("zxcvbnmjguyt"),
  ("dhjakshduh123"),
  ("djsahdkj@1"),
  ("dsajhd@kgljfdl"),
  ("mbjghmf&%&hi"),
  ("fjlsdkjf^&"),
  ("fdksljfjgfdljk"),
  ("fdslkjglkgj^%&"),
  ("^&%^&%JKHUIYu");

INSERT INTO order_detail(start_point,end_point,city,price,payment,date_of_order,status_of_order, rating)
VALUES
	("Zelena","Bandery","Lviv",70,"cash","21082019",false,null),
	("Shota","Pulia","Lviv",100,"credit_card","26072019",false,null),
	("Yaroslava","Shevchenka","Lviv",37,"cash","1082019",false,3),
	("Zalizna","Shota","Kyiv",63,"cash","9082019",false,null),
	("Tarnavskogo","Bandery","Lviv",55,"credit_card","10082019",false,4),
	("Medyka","Bandery","Dnipro",200,"cash","17082019",false,null),
	("Oliseha","Mogylyanka","Lviv",111,"cash","12092019",false,2),
	("Mudrogo","Zelena","Kyiv",120,"cash","13092019",false,null),
	("Kubryka","Vygovskogo","Kyiv",78,"credit_card","14092019",false,5),
	("Bandery","Geroiv UPA","Dnipro",34,"cash","15092019",false,5),
	("Shota","Zelena","Lviv",67,"cash","30102019",false,2),
	("ForumLviv","King Kross","Lviv",90,"cash","30102019",false,4),
	("Franka","Lypynskogo","Lviv",30,"credit_card","31102019",true,1),
	("Shevchenka","Chornovola","Lviv",40,"cash","31102019",true,4),
	("Pylia","Topolna","Lviv",150,"credit_card","31102019",true,4);

INSERT INTO customer(customer_name,phone_number,password_id,credit_card_id)
VALUES
	("Dima","0658763487", 1, 1),
	("Andrii","0678657895", 2, 2),
	("Vladyslav","0967587645", 3, 3),
	("Olena","0634275531", 4, null),
	("Nastya","06758745", 5, null);
INSERT INTO customer_has_order_detail(customer_id, order_detail_id) 
VALUES
	(1,3),(1,2),(2,1),(5,4),(2,5),(3,6),(1,7),(5,8);
    
	/* DRIVER INSERT */
    
INSERT INTO driver(driver_name,phone_number,driver_status,average_rating,credit_card_id,password_id)
VALUES
	("Mykola","0645786576",false,3,4,7),
	("Andrii","0676548763",true,4,5,6),
	("Volodya","0654782545",true,5,7,9),
	("Oleksandr","0789012345",false,2,6,8),
	("Dmytro","0634567123",true,4,8,10);
    
    /* CAR INSERT */
    
INSERT INTO car(brand,model,class,driver_id)
VALUES
	("AUDI","A6","business",1),
	("VOLKSWAGEN","JETTA","comfort",2),
	("VOLKSWAGEN","GOLF","comfort",2),
	("VOLKSWAGEN","T4","minibus",5),
	("VOLKSWAGEN","PASSATB7","comfort",3),
	("RENO","MEGAN","comfort",4);
    
    /* DRIVER_HAS_ORDER_DETAIL INSERT */

INSERT INTO driver_has_order_detail(driver_id,order_detail_id)
VALUES
	(1,1),(1,2),(2,3),(3,4),(2,5),(5,6),(5,7),(4,8),
    (4,9),(1,10),(3,11),(1,12),(2,13),(3,14),(5,15);
    
    /* BUSINESS_CUSTOMER INSERT */
    
INSERT INTO business_customer(business_customer_name,phone_number,credit_card_id,password_id)
VALUES
	("NESTLE", "0673864735",9,11),
	("LEXOPOLIS", "0678654763",11,15),
	("OXFORDMEDICAL", "0634275935",13,13),
	("FUJIKURA", "0678563821",12,12),
	("HYDROCYLINDER", "0765846492",10,14);
    
    /* BUSINESS_CUSTOMER_HAS_ORDER_DETAIL INSERT */
    
INSERT INTO business_customer_has_order_detail(business_customer_id,order_detail_id)
VALUES
	(1,15),(1,11),(2,9),(5,10),(5,14),(3,12),(4,13);
    
    /* REPORT INSERT */
    
INSERT INTO report(report_name,driver_id)
VALUES
	("very rough", 2),
	("smoking in car", 3),
	("fast driving", 3),
	("fast driving", 1),
	("very rough", 2);
    
    /* REPORT INSERT */
    
INSERT INTO incident(incident_name,car_id,incident_status)
VALUES
	("hitting a pedestrian",1,false),
	("road accident", 2 ,true),
	("speeding",1  , false),
	("speeding",2 , true),
	("drove into the red light",6 , false);